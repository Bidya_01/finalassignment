﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Component2_c7202629
{
    //Factory class to get  required shape.
    class ShapeFactory
    {
        //Method to get require shape.
        public Shape GetShape(string shapeType)
        {
            //block of code to be executed  if the condition is true.
            if (shapeType == "circle")
            {
                return new Circle();
            }
            else if (shapeType == "rectangle")
            {
                return new Rectangle();
            }

            else if (shapeType == "triangle")
            {
                return new Triangle();
            }
            else if (shapeType == "square")
            {
                return new Square();
            }
            return null;
        }
    }
}
