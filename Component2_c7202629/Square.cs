﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Component2_c7202629
{
    /// <summary>
    /// class to draw Square
    /// </summary>
    class Square : Shape
    {

        private int length, x, y;
        /// <summary>
        /// used to call the interget values of x,y
        /// used to pass value of square
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>

        public void saved_values(int a, int b, int c)
        {
            x = a;
            y = b;
            length = c;
        }
        /// <summary>
        /// Draw a shape of over the square using pen 
        /// </summary>
        /// <param name="g"></param>
        public void Draw_shape(Graphics g)
        {

            Pen mew2 = Form1.defaultpen;
            SolidBrush me = Form1.sb;
            if (Form1.fill)
            {
                g.FillRectangle(me, x, y, length, length);     //gives filled ellipse 
            }
            else
            {
                g.DrawRectangle(mew2, x, y, length, length);     //gives not filled ellipse
            }

        }


    }
}
