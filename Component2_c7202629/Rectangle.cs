﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Component2_c7202629
{
    /// <summary>
    /// class to draw Rectangle
    /// </summary>
    class Rectangle:Shape
    {
        private int x, y, w, h;
        /// <summary>
        /// integer values of x-axis, y_axis , width and height of rectangle is decleared
        /// </summary>
        /// <param name="a">origin x- coordinate</param>
        /// <param name="b">origin y- corordinate</param>
        /// <param name="c">width of the rectangle</param>
        /// <param name="d">height of the rectangle</param>
        public void values_given(int a, int b, int c, int d)
        {
            x = a;
            y = b;
            w = c;
            h = d;
        }
        /// <summary>
        /// used in drawing rectangle on provided points
        /// </summary>
        /// <param name="g">object for graphics</param>
        
       public void Draw_shape(Graphics g)
        {
            //Pen mew3 = new Pen(Color.Blue, 2);
            //g.DrawRectangle(mew3, x, y, w, h);

            Pen pen = Form1.defaultpen;    //pen set to deafault. i.e. black   
            SolidBrush brush = Form1.sb;   //brush set to deafault. i.e. black   
            if (Form1.fill)
            {
                g.FillRectangle(brush, x, y, w, h);    //filled rectangle drawn
            }
            else
            {
                g.DrawRectangle(pen, x, y, w, h);      //not filled rectangle drawn
            }



        }

        /// <summary>
        /// used in drawing rectangle on provided points
        /// </summary>
        /// <param name="g"></param>
        


        }
}
