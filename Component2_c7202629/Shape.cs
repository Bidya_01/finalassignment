﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Component2_c7202629
{
    /// <summary>
    /// parent class for all the shapes and drawing
    /// </summary>
   interface Shape
    {
        /// <summary>
        /// method for overriding
        /// </summary>
        /// <param name="g">object for Graphics</param>
        void Draw_shape(Graphics g);
    }
}
