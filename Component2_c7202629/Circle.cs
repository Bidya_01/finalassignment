﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Component2_c7202629
{
    /// <summary>
    /// class to draw circle
    /// </summary>
    class Circle : Form1, Shape
    {
        private int r, x, y;
        /// <summary>
        /// used to call the interget values of r,x, y
        /// </summary>
        /// used to pass value of circle
        /// <param name="a">origin x- coordinate</param>
        /// <param name="b">origin y- coordinate</param>
        /// <param name="c">radius of circle</param>
        public void saved_values(int a, int b, int c)
        {
            x = a;
            y = b;
            r = c;
        }
        /// <summary>
        /// used to draw the circle on panel
        /// </summary>
        /// <param name="g"></param>
       public void Draw_shape(Graphics g)
        {
            //Pen mew2 = new Pen(Color.Black,2);
            // g.DrawEllipse(mew2, x, y, r, r);



            Pen mew2 = Form1.defaultpen;
            SolidBrush me = Form1.sb;
            if (Form1.fill)
            {
                g.FillEllipse(me, x, y, r, r);     //gives filled ellipse 
            }
            else
            {
                g.DrawEllipse(mew2, x, y, r, r);     //gives not filled ellipse
            }

        }
        /// <summary>
        /// Draw a shape of over the circle  using pen 
        /// </summary>
        /// <param name="g"></param>
        
    }
}
